<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\QuestionnaireController;
use App\Http\Controllers\SubscriberController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::apiResource('applications', ApplicationController::class);
// Route::apiResource('questionnaires', QuestionnaireController::class);
// Route::apiResource('subscribers', SubscriberController::class);

Route::get('/applications', [ApplicationController::class, 'index'])->name('application.index');
Route::post('/applications', [ApplicationController::class, 'store'])->name('application.store');
Route::get('/questionnaires', [QuestionnaireController::class, 'index'])->name('questionnaire.index');
Route::post('/questionnaires', [QuestionnaireController::class, 'store'])->name('questionnaire.store');
Route::get('/subscribers', [SubscriberController::class, 'index'])->name('subscriber.index');
Route::post('/subscribers', [SubscriberController::class, 'store'])->name('subscriber.store');
