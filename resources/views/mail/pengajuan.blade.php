<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
</head>

<body>
    <p>Pengajuan Anda berhasil terkirim. Selanjutnya Anda dapat menunggu pihak kami untuk menghubungi Anda atau Anda
        dapat langsung menghubungi admin kami melalui Whatsapp dengan klik tombol di bawah ini.
    </p>
    <a href="https://wa.me/6283829253545?text={{ urlencode($text) }}"><button>Hubungi Admin</button></a>
</body>

</html>
