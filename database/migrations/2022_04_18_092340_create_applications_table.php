<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('phone_number');
            $table->string('email');
            $table->string('domicile');
            $table->enum('warranty', ['milik pribadi','pihak kedua']);
            $table->string('vehicle_brand');
            $table->string('vehicle_model');
            $table->enum('fuel_type', ['bensin', 'solar']);
            $table->enum('transmission_type', ['automatic', 'manual']);
            $table->enum('wheel_drive_type', ['4x2', '4x4']);
            $table->year('vehicle_year');
            $table->string('license_plate');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
