<?php

namespace App\Http\Controllers;

use App\Models\Questionnaire;
use Illuminate\Http\Request;

class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questionnaires = Questionnaire::all();
        return response([
            'status'=>'success',
            'questionnaires'=>$questionnaires
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'rate' => 'required|integer|min:1|max:5',
            'recommendation' => 'required|string',
            'feature' => 'required|string',
            'email' => 'required|string|email',
        ]);

        if (!($request->recommendation == 'yes'||$request->recommendation == 'no'||$request->recommendation == 'maybe')) {
            return response([
                'status' => 'error',
                'message' => 'Invalid input for recommendation'
            ]);
        } else {
            $questionnaire = Questionnaire::create($request->all());

            return response([
                'status' => 'success',
                'data' => $questionnaire
            ]);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function show(Questionnaire $questionnaire)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Questionnaire $questionnaire)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questionnaire $questionnaire)
    {
        //
    }
}
