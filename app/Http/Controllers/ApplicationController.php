<?php

namespace App\Http\Controllers;

use App\Models\Application;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications = Application::all();
        return response([
            'status'=>'success',
            'applications'=>$applications
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'phone_number' => 'required|string',
            'email' => 'required|string|email',
            'domicile' => 'required|string',
            'warranty' => 'required|string',
            'vehicle_brand' => 'required|string',
            'vehicle_model' => 'required|string',
            'fuel_type' => 'required|string',
            'transmission_type' => 'required|string',
            'wheel_drive_type' => 'required|string',
            'vehicle_year' => 'required|integer',
            'license_plate' => 'required|string'
        ]);
        
        if(!($request->warranty == 'milik pribadi' || $request->warranty == 'pihak kedua')) {
            return response([
                'status' => 'error',
                'message' => 'Invalid input for Dokumen Jaminan'
            ]);
        };
        
        if(!($request->fuel_type == 'bensin' || $request->fuel_type == 'solar')) {
            return response([
                'status' => 'error',
                'message' => 'Invalid input for Bahan Bakar Kendaraan'
            ]);
        };
        
        if(!($request->transmission_type == 'automatic' || $request->transmission_type == 'manual')) {
            return response([
                'status' => 'error',
                'message' => 'Invalid input for Transmisi Kendaraan'
            ]);
        };
        
        if(!($request->wheel_drive_type == '4x2' || $request->wheel_drive_type == '4x4')) {
            return response([
                'status' => 'error',
                'message' => 'Invalid input for Jenis Roda Penggerak'
            ]);
        };
        
        $application = Application::create($request->all());

        $mail_data = [
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'domicile' => $request->domicile,
            'warranty' => $request->warranty,
            'vehicle_brand' => $request->vehicle_brand,
            'vehicle_model' => $request->vehicle_model,
            'fuel_type' => $request->fuel_type,
            'transmission_type' => $request->transmission_type,
            'wheel_drive_type' => $request->wheel_drive_type,
            'vehicle_year' => $request->vehicle_year,
            'license_plate' => $request->license_plate,
            'text' => "Halo Momo, saya ingin mengajukan pinjaman dana tunai\nBerikut saya sertakan beberapa informasi tentang diri saya\nNama Lengkap : $request->name\nNomor Telepon : $request->phone_number\nAlamat Email : $request->email\nDomisili : $request->domicile\nDokumen Jaminan : BPKB Atas Nama ".ucwords($request->warranty)."\nMerk Kendaraan : $request->vehicle_brand\nModel Kendaraan : $request->vehicle_model\nBahan Bakar Kendaraan : ".ucfirst($request->fuel_type)."\nTransmisi Kendaraan : ".ucfirst($request->transmission_type)."\nJenis Roda Penggerak : $request->wheel_drive_type\nTahun Kendaraan : $request->vehicle_year\nNomor Kendaraan Bermotor : $request->license_plate"
        ];

        \Mail::send('mail.pengajuan', $mail_data, function($message) use ($mail_data) {
            $message->to($mail_data['email'])
            ->subject('Pengajuan Terkirim');
        });

        return response([
            'status' => 'success',
            'data' => $application
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $application = Application::find($id);
        if (!$application) {
            return response(['status'=> 400, 'message'=>'Pengajuan dengan id tersebut tidak dapat ditemukan']);
        } else {
            return response(['status'=> 200, 'application'=>$application]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Application $application)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function destroy(Application $application)
    {
        //
    }
}
