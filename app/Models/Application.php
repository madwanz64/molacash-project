<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory; 
    protected $fillable = ['name','phone_number','email','domicile','warranty','vehicle_brand','vehicle_model','fuel_type','transmission_type','wheel_drive_type','vehicle_year','license_plate'];
}
